import React from "react";
import {BrowserRouter as Router, Routes as Links, Route} from "react-router-dom";
import Login from "../views/Login";
import Home from "../views/Home";
import NewUser from "../views/NewUser";

function Routes(){
    return(
        <Router>
            <Links>
                <Route exact path="/" element={<Login />} />
                <Route exact path="/home" element={<Home />} />
                <Route exact path="/newUser" element={<NewUser />} />
            </Links>
        </Router>
    );
}

export default Routes;
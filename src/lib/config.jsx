import Cookies from 'universal-cookie';
export const cookies = new Cookies();

export const API = {
    urlApi:'http://qa-nexura.com/api/',
    urlSocket:'http://qa-nexura.com/',
    title:'API REST NEXURA®'
}

export const COOKIES = cookies;

export const headers = {
    headers: {
        'Access-Control-Allow-Origin': '*', 
        'Access-Control-Allow-Headers':'Origin, X-Requested-With, Content-Type, Accept'}
}

export const headersSxToken = {
    headers: {'nxtoken':cookies.get('token'),'sxToken': 'a24e89eb2fb7fe151bc6e19d81d705bd'}
}
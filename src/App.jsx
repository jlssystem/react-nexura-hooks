import SidebarNavbar from './components/layouts/SidebarNavbar';
import ScrollTop from './components/layouts/ScrollTop';
import {SidebarContentWrapper} from './components/layouts/SidebarContentWrapper';
function dahsboard() {
  const sidebarContentWrapperData = {
    title:'CRUD DE REACT JS',
    list:['Home', 'tables', 'Simple Tables']
  }
  const dataExample = 'Datos de ejemplo de como pasar un valor entre componentes';
  return (
    <div id="wrapper">
      <SidebarNavbar />
      <SidebarContentWrapper {...sidebarContentWrapperData} dataExample={dataExample} />
      <ScrollTop />
    </div>
  );
}

function login(){
  return (
    <div>
      <h1>Login</h1>
    </div>
  );
}

function App(){
  return (<>
  <h1>Nueva interfaz</h1>
  </>);
}

export default App;

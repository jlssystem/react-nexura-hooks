import React, { Component } from 'react';
import "../assets/css/formLogin.css";
import axios from "axios";

class Login extends Component {

    constructor(){
        super();
        this.state = ({
            form:{
                login:'',
                password:''
            },
            error:false,
            errorMessage:'',
            title:'',
            theme:'alert-danger'
        });
    }
    

    handleChange = async (event) =>{
        await this.setState({
            form:{
                ...this.state.form,
                [event.target.name]:event.target.value
            }
        });
    }


    getMessage =(message)=> {
        this.setState({
            error:true,
            errorMessage: message
        });                
        setTimeout(()=>{
            this.setState({error:false});
        }, 5000);  
    }

        handleSubmit = (event) => {
            event.preventDefault();
            console.log('enviando', this.state.form);
            axios.post('http://qa-nexura.com/api/usuarios/login', this.state.form).then(response => {   
                console.log('response', response);                 
            }).catch(error => {
                console.log('Error 0001x Send form', error);
            });        
        }

    render(){
        return (
            <React.StrictMode>
                <div className="wrapper fadeInDown pt-5">
                    <div id="formContent">
                        <div className="fadeIn first">
                            <i className="fa fa-user fa-2x pt-3"></i>
                        </div>
                        <form onSubmit={this.handleSubmit}>
                            <input type="text" id="login"  className="fadeIn second"  name="login" onChange={this.handleChange} placeholder="login" />
                            <input type="text" id="password" className="fadeIn third" name="password" onChange={this.handleChange} placeholder="password" />
                            <input type="submit" className="fadeIn fourth" value="Log In" />
                        </form>
                        <div id="formFooter">
                            <a className="underlineHover" href="#">Forgot Password?</a>
                        </div>    
                    </div>
                </div>
            </React.StrictMode>
        );
    }
}

export default Login;

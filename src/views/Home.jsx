import React, {useEffect, useState} from "react";
import SidebarNavbar from '../components/layouts/SidebarNavbar';
import ScrollTop from '../components/layouts/ScrollTop';
import {SidebarContentWrapper} from '../components/layouts/SidebarContentWrapper';
import {cookies, headersSxToken} from "../lib/config";
import  DataTableUsuario  from '../components/datatables/DataTableUsuario';

function Home() {
  const sidebarContentWrapperData = {
    title:'CRUD DE REACT JS',
    list:['Home', 'tables', 'Simple Tables']
  }

  useEffect(()=>{
        console.log('se ejecuta una sola vez renderizando ...');
        if(!cookies.get('token')){
            window.location.href="./";
        }
  }, []);

  const dataExample = 'Datos de ejemplo de como pasar un valor entre componentes';

  return (
    <div id="wrapper">
      <SidebarNavbar />
      <SidebarContentWrapper {...sidebarContentWrapperData} content={<DataTableUsuario type="basicTop"/>} dataExample={dataExample} />
      <ScrollTop />
    </div>
  );
}

export default Home;
import React, {useEffect, useState} from "react";
import "../assets/css/formLogin.css";
import axios from "axios";
import { Alert } from "../components/bootstrap";
import { API, cookies, headersSxToken} from "../lib/config";


const Login =()=>{
    const [form, setForm] = useState({login: '', password: '', sxToken:'a24e89eb2fb7fe151bc6e19d81d705bd'});
    const [alerta, setAlerta] = useState({isAlert:false, text:'', color:'defalut'}); 

    useEffect(()=>{
        console.log('se ejecuta una sola vez al inicio del renderizando.');
        if(cookies.get('token')){
            window.location.href="./home";
        }
    }, []);

    useEffect(()=>{
        console.log('se ejecuta cuando se actualiza el componente');
    });

    const handleListNoAuth = async (event)=>{
        event.preventDefault();
        await axios.get(API.urlApi+'usuarios/listar2').then(response=>{
            console.log('response handleListNoAuth:', response.data.data);
        });
    }

    const handleSubmit = async (event)=>{
        event.preventDefault();
        await axios.get(API.urlApi+'usuarios/loginApi?login='+form.login+'&password='+form.password).then(response=>{
            const data =  JSON.parse(response.data.data);           
            if(data.status==200){
                cookies.set('id', data.data.id, {path: "/"});
                cookies.set('nombre', data.data.nombre, {path: "/"});
                cookies.set('email', data.data.email, {path: "/"});
                cookies.set('nombre', data.data.nombre, {path: "/"});
                cookies.set('token', data.data.token, {path: "/"});
                window.location.href="./home";
            }else if(data.status==401){
                setAlerta({isAlert:true, text:data.error.message, color:'danger'});
                setTimeout(function(){
                    setAlerta({isAlert:false});
                }, 3000);
            }
        });
    }


    const handleSubmitPost = async (event)=>{
        event.preventDefault();
        await axios.post(API.urlApi+'usuarios/login', form, headersSxToken).then(response=>{
            console.log('response:', response.data.data);
            const data =  JSON.parse(response.data.data);           
            /*if(data.status==200){
                console.log('Nombre:', data.data.nombre);
                cookies.set('id', data.data.id, {path: "/"});
                cookies.set('nombre', data.data.nombre, {path: "/"});
                cookies.set('email', data.data.email, {path: "/"});
                cookies.set('nombre', data.data.nombre, {path: "/"});
                cookies.set('token', data.data.token, {path: "/"});
                window.location.href="./home";
            }else if(data.status==401){
                console.log(data.error.message);
                setAlerta({isAlert:true, text:data.error.message, color:'danger'});
                setTimeout(function(){
                    setAlerta({isAlert:false});
                }, 3000);
            }*/
        });
    }

   const handleChange =(event)=> {
        //console.log('Actualizando input', form);
        setForm({...form, [event.target.name]:event.target.value});
    }

    return (
        <React.StrictMode>
            <div className="wrapper fadeInDown pt-5" >
                <div id="formContent">
                    <div className="fadeIn first">
                        <i className="fa fa-user fa-2x pt-3"></i>
                    </div>
                    {(alerta.isAlert == true) &&
                        <Alert text={alerta.text} color={alerta.color} />
                    }                     
                    <form onSubmit={handleSubmit}>
                        <input type="text" id="login"  className="fadeIn second inputLoginText"  name="login" onChange={handleChange} placeholder="login" />
                        <input type="password" id="password" className="fadeIn third inputLoginPass" name="password" onChange={handleChange} placeholder="password" />
                        <input type="submit" className="fadeIn fourth" value="Log In" />
                    </form>
                    <div id="formFooter">
                        <a className="underlineHover" href="#" onClick={handleListNoAuth}>Forgot Password?</a>
                    </div>
                </div>
            </div>
        </React.StrictMode>
    );
}

export default Login;
import React, {useEffect, useState} from "react";
import { ModalLink } from "../Modal";
import {FormNewUsuario, FormUpdateUsuario} from "../forms/FormsUsuario";
import {ConfirmButton} from "../bootstrap";
import { API, headersSxToken } from "../../lib/config";
import axios from "axios";
import { MDBDataTableV5 } from 'mdbreact';

//https://mdbootstrap.com/docs/react/tables/datatables/#introduction

const dataColumn = 
    [
      {
        label: 'Nº',
        field: 'num',
        width: 270,
      },
      {
        label: 'Nombre',
        field: 'nombre',
        width: 150,
        attributes: {
          'aria-controls': 'DataTable',
          'aria-label': 'Name',
        },
      },
      {
        label: 'Login',
        field: 'login',
        width: 270,
      },
      {
        label: 'Email',
        field: 'email',
        width: 270,
      },
      {
        label: 'Dirección',
        field: 'direccion',
        width: 270,
      },
      {
        label: 'Teléfono',
        field: 'telefono',
        width: 200,
      },
      {
        label: 'Acciones',
        field: 'actions',
        width: 200,
      }
    ];

const Basic = (props) => {
    const [datatable, setDatatable] = useState({});

    const actionDelete = async (id) =>{
      await axios.get(API.urlApi+'usuarios/userDelete', { params: { id:id } } ).then(response => {
         let data = JSON.parse(response.data.data); 
         if(parseInt(data.status)===200){
          getData();
         }else if(parseInt(data.status)===500){
          console.log('error 500');
         }
      });
    }

    const callbackDelete = (confirm, id) =>{
      if(confirm){
        actionDelete(id);
      }
    }

    const add = (obj) =>{
      let i=0;
      for(let item of obj){
        i++;
        item.num=i;
        item.actions =  <>
                          <ModalLink body={<FormUpdateUsuario data={item} callbackGetData={getData} />} class="btn btn-success text-white" show={false} backdrop="static" size="xl" icono="fa fa-edit" title="Editar usuario" />
                          <ConfirmButton class="btn btn-danger ml-3" title="Eliminar registro" fa="fa fa-trash" id={item.id} callbackDelete={callbackDelete} />
                        </>
      }
    }

    const preGetData = (data) =>{
      add(data);
      setDatatable({
        columns: dataColumn,
        rows: data
      });
    }

    const getData = async () => {
      await axios.get(API.urlApi+'usuarios/userAll').then(response => {
         preGetData(JSON.parse(response.data.data));
      });
    }

    useEffect(()=>{ 
      getData();
    }, []);

    return <>
          <div className="card-header py-3 d-flex flex-row align-items-center justify-content-between">
            <h6 className="m-0 font-weight-bold text-primary">                  
              <ModalLink body={<FormNewUsuario callbackGetData={getData} />} class="btn btn-success text-white" text="Nuevo usuario" show={false} backdrop="static" size="xl" icono="fa fa-user-plus" title="Nuevo usuario" />
            </h6>
          </div>
          {(props.type=='' || props.type=='basicTop')?
            <MDBDataTableV5
              hover
              entriesOptions={[5, 20, 25]}
              entries={5}
              pagesAmount={4}
              data={datatable}
              pagingTop
              searchTop
              searchBottom={false}
              barReverse
            />
            :
            (props.type=='basicButtom')?
              <MDBDataTableV5 hover order={['age', 'desc']} data={datatable} />
            :
            (props.type=='basicButtom2')?
              <MDBDataTableV5 hover entriesOptions={[5, 20, 25]} entries={5} pagesAmount={4} data={datatable} searchTop searchBottom={false} />
            :
            <MDBDataTableV5 hover entriesOptions={[5, 20, 25]} entries={5} pagesAmount={4} data={datatable} materialSearch />
          }
    </>;
}

export default Basic;
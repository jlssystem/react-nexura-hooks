import React, {Component} from "react";
import axios from "axios";
import uuid from 'react-uuid';
import { API, headersSxToken } from "../../lib/config";
import { ModalLink } from "../Modal";
import {FormNewUsuario, FormUpdateUsuario} from "../forms/FormsUsuario";
import {ConfirmButton} from "../bootstrap";

//Datatable Modules
import "datatables.net-dt/js/dataTables.dataTables"
import "datatables.net-dt/css/jquery.dataTables.min.css"
import $ from 'jquery'; 


class DataTablesUsuarios extends Component {
  constructor(props){
    super(props);
    this.state =  {datos:[], id:'dataTable'}
   }

  actualizar(data){
    this.setState({datos:data});
  } 
  
  datatable(isMontar) {
    var self = this;
    if(isMontar){
      $('#'+this.state.id).dataTable({
            "bAutoWidth": false,
            "bDestroy": true,		
            "fnDrawCallback": function() {		  		
                  self.forceUpdate();        	
            }, 
      });
    }else{
        $('#'+this.state.id).dataTable({
            "bAutoWidth": false,
            "bDestroy": true,	
        });
    } 		
  }

  componentDidMount() {
    this.getData()
    this.datatable(true);
    this.setState({id:this.props.id});
  }
  
  componentDidUpdate(){
    this.datatable(false);
 }

  async getData(){    
    await axios.get(API.urlApi+'usuarios/userAll').then(response => {
      this.actualizar(JSON.parse(response.data.data));
    });
  }

  async actionDelete(id){
    await axios.get(API.urlApi+'usuarios/userDelete', { params: { id:id } } ).then(response => {
       let data = JSON.parse(response.data.data); 
       console.log(data.data.id);
       if(parseInt(data.status)===200){
        $("#tr-"+data.data.id).hide();
       }else if(parseInt(data.status)===500){
        console.log('error 500');
       }
    });
  }

  callbackDelete = (confirm, id) =>{
      if(confirm){
        this.actionDelete(id);
      }
  }


  preRender = () =>{    
    return (<>
        <div className="row">
          <div className="col-lg-12">
            <div className="card mb-4">
              <div className="card-header py-3 d-flex flex-row align-items-center justify-content-between">
                <h6 className="m-0 font-weight-bold text-primary">                  
                  <ModalLink body={<FormNewUsuario />} class="btn btn-success text-white" text="Nuevo usuario" show={false} backdrop="static" size="xl" icono="fa fa-user-plus" title="Nuevo usuario" />
                </h6>
              </div>
              <div className="table-responsive p-3">
                <table className="table align-items-center table-flush" id={this.state.id}>
                  <thead className="thead-light">
                    <tr>
                      <th>Nombre</th>
                      <th>Login</th>
                      <th>Email</th>
                      <th>Dirección</th>
                      <th>Teléfono</th>
                      <th>Acciones</th>
                    </tr>
                  </thead>
                  <tfoot>
                    <tr>
                      <th>Nombre</th>
                      <th>Login</th>
                      <th>Email</th>
                      <th>Dirección</th>
                      <th>Teléfono</th>
                      <th>Acciones</th>
                    </tr>
                  </tfoot>
                  <tbody>
                    {this.state.datos.length &&                       
                       Object.values(this.state.datos).map((value, key) => {
                        return <tr key={uuid()} id={"tr-"+value.id}>
                                  <td>{value.nombre}</td>
                                  <td>{value.login}</td>
                                  <td>{value.email}</td>
                                  <td>{value.direccion}</td>
                                  <td>{value.telefono}</td>
                                  <td>
                                    <ModalLink body={<FormUpdateUsuario data={value} />} class="btn btn-success text-white" show={false} backdrop="static" size="xl" icono="fa fa-edit" title="Editar usuario" />
                                    <ConfirmButton class="btn btn-danger ml-3" title="Eliminar registro" fa="fa fa-trash" id={value.id} callbackDelete={this.callbackDelete} />
                                  </td>
                                </tr>
                      })
                    } 
                  </tbody>
                </table>
              </div>
            </div>
          </div>
        </div>
    </>);
  }

  render(){
    const { classes } = this.props;
    return(<>
            {
              (this.state.datos !==undefined && this.state.datos.length>0)?this.preRender():<li>No hay</li>
            }
        </>           
    )
  }
}
export default DataTablesUsuarios;

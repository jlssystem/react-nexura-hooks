import React from "react";
import {confirm} from 'react-bootstrap-confirmation';

export const Alert = (props) =>{
    return (<>
        <div className={"alert alert-"+ props.color}>{props.text}</div>
    </>);
}

export const ConfirmButton = (props) => {
  const display = async () => {
    const result = await confirm('¿Esta seguro que desea realizar esta acción?');
    props.callbackDelete(result, props.id);
  };
  return (
    <button type="button" className={props.class} title={props.title} onClick={display}>
      <i className={props.fa}></i>
    </button>
  );
};
import React, {useEffect, useState} from "react";
import {API, cookies, headersSxToken} from "../../lib/config";
import { Alert } from "../../components/bootstrap";
import axios from "axios";

export const FormNewUsuario = (props) => {
    const [form, setForm] = useState({name: '', email: '', login:'', type:'CMS', sxToken:cookies.get('token')});
    const [alerta, setAlerta] = useState({isAlert:false, text:'', color:'defalut'}); 

    const handleSubmitbk = async (event) => {
        event.preventDefault();
        await axios.post(API.urlApi+'usuarios/userNew', [form.name, form.email, form.login, form.type, form.sxToken], {
            headers: {
                nxtoken:cookies.get('token')
            }
          }).then(response =>{ 
            console.log('Guardando usuario:', response);
        });
    }

    const handleClear = (event) =>{
        document.getElementById('formUsuario').reset();
        setForm({name: '', email: '', login:''});
    }

    const status = data =>{
        if(data.status == 400){
            console.log('mensaje:', data.error.message);
            setAlerta({isAlert:true, text:data.error.message, color:'danger'});
            setTimeout(function(){
                setAlerta({isAlert:false});
            }, 3000);
        }else if(data.status == 500){
            setAlerta({isAlert:true, text:data.error.message, color:'danger'});
            setTimeout(function(){
                setAlerta({isAlert:false});
            }, 3000);
        }else if(data.status == 200){
            setAlerta({isAlert:true, text:'Datos guardados correctamente', color:'success'});                
            if(props){
                setTimeout(function(){
                    setAlerta({isAlert:false});
                }, 3000);               
                handleClear();
                props.callbackGetData();
            }else{
                setTimeout(function(){
                    setAlerta({isAlert:false});
                    window.location.href='NewUser';
                }, 3000);               
                handleClear();
            }
        }else{
            console.log('error'); 
        }
    }

    const handleSubmit = async (event) => {
        event.preventDefault();
        await axios.get(API.urlApi+'usuarios/userNew', {params: {name:form.name, login:form.login, email:form.email, type:form.type}}).then(response =>{ 
            const data =  JSON.parse(response.data.data); 
            status(data);
        });
    }

    const handleChange = (event) =>{
        setForm({...form, [event.target.name]:event.target.value});
    }

    return (<>
            {(alerta.isAlert == true) &&
                <Alert text={alerta.text} color={alerta.color} />
            }
            <form id="formUsuario" name="formUsuario" onSubmit={handleSubmit}>
                <div className="form-group">
                    <label htmlFor="exampleInputNombre">Nombre</label>
                    <input type="text" className="form-control" onChange={handleChange} id="exampleInputNombre" name="name" aria-describedby="emailHelp" placeholder="Ingresar nombre"/>
                    <small id="emailHelp" className="form-text text-muted"></small>
                </div>
                <div className="form-group">
                    <label htmlFor="exampleInputLogin">Login</label>
                    <input type="text" className="form-control" onChange={handleChange} id="exampleInputLogin" name="login" aria-describedby="emailHelp" placeholder="Ingresar login"/>
                    <small id="emailHelp" className="form-text text-muted"></small>
                </div>
                <div className="form-group">
                    <label htmlFor="exampleInputEmail">Email</label>
                    <input type="email" className="form-control" onChange={handleChange} id="exampleInputEmail" name="email" aria-describedby="emailHelp" placeholder="Ingresar email"/>
                    <small id="emailHelp" className="form-text text-muted"></small>
                </div>            
                <button type="submit" className="btn btn-primary">Enviar</button>   
            </form>
    </>);
}

export const FormUpdateUsuario = (props) => {
    const [form, setForm] = useState({id: props.data.id, name: props.data.nombre, email: props.data.email, login:props.data.login, type:'CMS', sxToken:cookies.get('token')});
    const [alerta, setAlerta] = useState({isAlert:false, text:'', color:'defalut'}); 

    const handleSubmitbk = async (event) => {
        event.preventDefault();
        await axios.post(API.urlApi+'usuarios/userUpdate', [form.name, form.email, form.login, form.type, form.sxToken], {
            headers: {
                nxtoken:cookies.get('token')
            }
          }).then(response =>{ 
            console.log('Guardando usuario:', response);
        });
    }

    const status = data => {
        if(data.status == 400){
            console.log('mensaje:', data.error.message);
            setAlerta({isAlert:true, text:data.error.message, color:'danger'});
            setTimeout(function(){
                setAlerta({isAlert:false});
            }, 3000);
        }else if(data.status == 500){
            setAlerta({isAlert:true, text:data.error.message, color:'danger'});
            setTimeout(function(){
                setAlerta({isAlert:false});
            }, 3000);
        }else if(data.status == 200){
            setAlerta({isAlert:true, text:'Datos guardados correctamente', color:'success'});
            setTimeout(function(){
                setAlerta({isAlert:false});
            }, 3000);
            props.callbackGetData()
        }else{
            console.log('error'); 
        }
    }

    const handleSubmit = async (event) => {
        event.preventDefault();
        await axios.get(API.urlApi+'usuarios/userUpdate', { params: { name:form.name, id:form.id, login:form.login, email:form.email, type:form.type} }).then(response =>{ 
            const data =  JSON.parse(response.data.data); 
            status(data);
        });
    }

    const handleChange = (event) =>{
        setForm({...form, [event.target.name]:event.target.value});
    }

    const handleClear = (event) =>{
        document.getElementById('formUsuario').reset();
        setForm({name: '', email: '', login:''});
    }

    return (<>
            {(alerta.isAlert == true) &&
                <Alert text={alerta.text} color={alerta.color} />
            }
            <form id="formUsuario" name="formUsuario" onSubmit={handleSubmit}>
                <div className="form-group">
                    <label htmlFor="exampleInputNombre">Nombre</label>
                    <input type="text" value={form.name} className="form-control" onChange={handleChange} id="exampleInputNombre" name="name" aria-describedby="emailHelp" placeholder="Ingresar nombre"/>
                    <small id="emailHelp" className="form-text text-muted"></small>
                </div>
                <div className="form-group">
                    <label htmlFor="exampleInputLogin">Login</label>
                    <input type="text" value={form.login} className="form-control" onChange={handleChange} id="exampleInputLogin" name="login" aria-describedby="emailHelp" placeholder="Ingresar login"/>
                    <small id="emailHelp" className="form-text text-muted"></small>
                </div>
                <div className="form-group">
                    <label htmlFor="exampleInputEmail">Email</label>
                    <input type="email" value={form.email} className="form-control" onChange={handleChange} id="exampleInputEmail" name="email" aria-describedby="emailHelp" placeholder="Ingresar email"/>
                    <small id="emailHelp" className="form-text text-muted"></small>
                </div>            
                <button type="submit" className="btn btn-primary">Actulizar</button>   
            </form>
    </>);
}
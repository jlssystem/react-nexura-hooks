import uuid from 'react-uuid';
//import  DataTablesUsuarios  from '../datatables/DataTableUsuarioClass';

export function SidebarContentWrapper(props){
    let i = 0;
    return (<>
        <div id="content-wrapper" className="d-flex flex-column">
            <div id="content">
                <nav className="navbar navbar-expand navbar-light bg-navbar topbar mb-4 static-top">
                    <button id="sidebarToggleTop" className="btn btn-link rounded-circle mr-3">
                        <i className="fa fa-bars"></i>
                    </button>
                </nav>
                <div className="container-fluid" id="container-wrapper">
                    <div className="d-sm-flex align-items-center justify-content-between mb-4">
                        <h1 className="h3 mb-0 text-gray-800">{props.title}</h1>
                        <ol className="breadcrumb">
                        {props.list.map((item)=>{
                            return <li key={uuid()} className="breadcrumb-item"><a href="./">{item}</a></li>
                            })
                        }
                        </ol>
                    </div>                    
                    {props.content}                    
                </div>
            </div>
            {Footer({title:'copyright', link:{href:'https://indrijunanda.gitlab.io/', title:'nexura'}})}
        </div>
    </>);   

}

export const Footer = (props) => {
    return (<>
        <footer className="sticky-footer bg-white">
                <div className="container my-auto">
                <div className="copyright text-center my-auto">
                <span>{props.title} &copy; <script> document.write(new Date().getFullYear()); </script></span>                     
                     <b><a href={props.link.href} target="_blank">{props.link.title}</a></b>
                </div>
                </div>
        </footer>
    </>);
}
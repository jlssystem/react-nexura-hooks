import React, {useEffect, useState} from "react";
import {cookies} from "../../lib/config";

function SidebarNavbar (){
  const cerrarSesion = () => {
    cookies.remove('id', {path: "/"});
    cookies.remove('email', {path: "/"});
    cookies.remove('nombre', {path: "/"});
    cookies.remove('token', {path: "/"});
    window.location.href='./';
  }
  return (<ul className="navbar-nav sidebar sidebar-light accordion" id="accordionSidebar">
    <a className="sidebar-brand d-flex align-items-center justify-content-center" href="/#">
      <div className="sidebar-brand-icon">
        <img src="ruang-admin/img/logo/logo2.png" />
      </div>
      <div className="sidebar-brand-text mx-3">{cookies.get('nombre')}</div>
    </a>
    <hr className="sidebar-divider my-0"/>
    <li className="nav-item">
      <a className="nav-link" href="index.html">
        <i className="fas fa-fw fa-tachometer-alt"></i>
        <span>Dashboard</span></a>
    </li>
    <hr className="sidebar-divider"/>
    <div className="sidebar-heading">
      Features
    </div>
    <li className="nav-item">
      <a className="nav-link collapsed" href="#" data-toggle="collapse" data-target="#collapseBootstrap"
        aria-expanded="true" aria-controls="collapseBootstrap">
        <i className="far fa-fw fa-window-maximize"></i>
        <span>Usuarios</span>
      </a>
      <div id="collapseBootstrap" className="collapse" aria-labelledby="headingBootstrap" data-parent="#accordionSidebar">
        <div className="bg-white py-2 collapse-inner rounded">
          <h6 className="collapse-header">Options</h6>
          <a className="collapse-item" href="/home">Listado de usuarios</a>
          <a className="collapse-item" href="/NewUser">Nuevo usuario</a>
        </div>
      </div>
    </li>
    
    <li className="nav-item">
      <a className="nav-link" href="#" onClick={cerrarSesion}>
        <i className="fas fa-fw fa-chart-area"></i>
        <span>Salir</span>
      </a>
    </li>
    <hr className="sidebar-divider"/>
    <div className="version" id="version-ruangadmin"></div>
  </ul>);
}
export default SidebarNavbar;